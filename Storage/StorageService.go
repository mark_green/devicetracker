package storage

import (
    "github.com/gocql/gocql"
)

type StorageService struct {
    configCluster *gocql.ClusterConfig
    configSession *gocql.Session

    messagingCluster *gocql.ClusterConfig
    messagingSession *gocql.Session
}

func NewStorageService(configKeyspace string, messagingKeyspace string, clusterAddresses ...string) *StorageService {
    instance := new (StorageService)

    instance.configCluster = gocql.NewCluster(clusterAddresses...)
    instance.configCluster.Keyspace = configKeyspace
    instance.messagingCluster = gocql.NewCluster(clusterAddresses...)
    instance.messagingCluster.Keyspace = messagingKeyspace
        
    var err error
    instance.configSession, err = instance.configCluster.CreateSession()
    if err != nil {
        panic(err)
    }
    instance.messagingSession, err = instance.messagingCluster.CreateSession()
    if err != nil {
        panic(err)
    }

    return instance
}
