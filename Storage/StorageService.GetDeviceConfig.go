package storage

import (
	"bitbucket.org/mark_green/DeviceTracker/Model"
)

func (this *StorageService) GetDeviceConfig(deviceName string) model.DeviceConfig {
	iter := this.configSession.Query(`SELECT 
									gpx_filename_prefix,
									max_range_in_degrees_for_stop,
									required_points_to_transition
								FROM devices
								WHERE device_name = ?`,
						deviceName).Iter()
	defer func() {
	    if err := iter.Close(); err != nil {
	        panic(err)
	    }
	}()
	
    config := model.DefaultDeviceConfig(deviceName)

	iter.Scan(&config.GpxFilenamePrefix, &config.MaxRangeInDegreesForStop, &config.RequiredPointsToTransition)

	return config
}