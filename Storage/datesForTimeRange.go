package storage

import (
	"time"
)

func datesForTimeRange(from time.Time, to time.Time) []string {
	dates := make([]string, 0, 32) // it's a fair guess that 32+ days will rarely be spanned...
	for date := from; date.Equal(to) || date.Before(to); date = date.Add(time.Duration(24)*time.Hour) {
		next := len(dates)
		dates = dates[0:next+1]
		dates[next] = dateForTime(date)
	}
	return dates
}