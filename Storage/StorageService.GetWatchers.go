package storage

import (
    
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

func (this *StorageService) GetWatchers(deviceName string, watchName string) []model.Watcher {
	iter := this.configSession.Query(`SELECT person_name, person_email
								FROM watchers
								WHERE device_name = ? AND watch_name = ?`,
						deviceName, watchName).Iter()
	defer func() {
	    if err := iter.Close(); err != nil {
	        panic(err)
	    }
	}()
	
    watchers := make([]model.Watcher, 0, 64)
	
	var personName string
	var personEmail string
	for iter.Scan(&personName, &personEmail) {
    	next := len(watchers)
    	watchers = watchers[0:next+1]
        watchers[next].DeviceName = deviceName
        watchers[next].WatchName = watchName
        watchers[next].PersonName = personName
        watchers[next].PersonEmail = personEmail
    }

	return watchers
}
