package storage

import (
    
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

func (this *StorageService) GetWatches(deviceName string) []model.Watch {
	iter := this.configSession.Query(`SELECT device_name, watch_name, watch_type,
								latitude, longitude,
								range_in_degrees
								FROM watches
								WHERE device_name = ?`,
						deviceName).Iter()
	defer func() {
	    if err := iter.Close(); err != nil {
	        panic(err)
	    }
	}()
	
    watches := make([]model.Watch, 0, 64)

	var watchName string
	var watchType model.WatchType
	var latitude float64
	var longitude float64
	var rangeInDegrees float64
	for iter.Scan(&deviceName, &watchName, &watchType, &latitude, &longitude, &rangeInDegrees) {
    	next := len(watches)
    	watches = watches[0:next+1]
        watches[next].DeviceName = deviceName
        watches[next].WatchName = watchName
        watches[next].WatchType = watchType
        watches[next].Latitude = latitude
        watches[next].Longitude = longitude
        watches[next].RangeInDegrees = rangeInDegrees
    }

	return watches
}
