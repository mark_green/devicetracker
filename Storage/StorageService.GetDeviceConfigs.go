package storage

import (
	"bitbucket.org/mark_green/DeviceTracker/Model"
)

func (this *StorageService) GetDeviceConfigs() []model.DeviceConfig {
	iter := this.configSession.Query(`SELECT 
									device_name,
									gpx_filename_prefix,
									max_range_in_degrees_for_stop,
									required_points_to_transition
								FROM devices`).Iter()
	defer func() {
	    if err := iter.Close(); err != nil {
	        panic(err)
	    }
	}()
	
	deviceConfigs := make([]model.DeviceConfig, 0)
    config := model.DefaultDeviceConfig("")
	for iter.Scan(&config.DeviceName, &config.GpxFilenamePrefix, &config.MaxRangeInDegreesForStop, &config.RequiredPointsToTransition) {
		deviceConfigs = append(deviceConfigs, config)
	}

	return deviceConfigs
}
