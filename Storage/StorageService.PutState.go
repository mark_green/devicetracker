package storage

import (
	"bytes"
	"encoding/json"

    "bitbucket.org/mark_green/Messaging"
)

func (this *StorageService) PutState(processManagerType string, correlation string, state messaging.ProcessManagerState) {
	var stateAsJson bytes.Buffer
	encoder := json.NewEncoder(&stateAsJson)
	encoder.Encode(state)

	err := this.messagingSession.Query(`INSERT INTO state (type, correlation, json)
				VALUES (?, ?, ?)`,
				processManagerType, correlation, stateAsJson.String()).Exec()

	if err != nil {
		panic(err)
	}
}