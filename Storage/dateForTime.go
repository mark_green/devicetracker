package storage

import (
	"time"
)

func dateForTime(time time.Time) string {
	return time.Format("2006-01-02T15:04:05Z")[0:10]
}