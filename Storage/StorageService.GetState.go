package storage

import (
	"bytes"
	"encoding/json"
	"reflect"

    "bitbucket.org/mark_green/Messaging"
)

func (this *StorageService) GetState(processManagerType string, correlation string, state messaging.ProcessManagerState) messaging.ProcessManagerState {
	iter := this.messagingSession.Query(`SELECT json FROM state WHERE type=? AND correlation=?`,
				processManagerType, correlation).Iter()
	defer func() {
	    if err := iter.Close(); err != nil {
	        panic(err)
	    }
	}()

	tState := reflect.TypeOf(state)
	vNewState := reflect.New(tState)
	
	var stateAsJsonString string
	for iter.Scan(&stateAsJsonString) {
		var stateAsJson bytes.Buffer
		stateAsJson.Write([]byte(stateAsJsonString))
		decoder := json.NewDecoder(&stateAsJson)
		err := decoder.Decode(vNewState.Interface())
		if err != nil {
			panic(err)
		}
		return vNewState.Elem().Interface().(messaging.ProcessManagerState)
	}
	return state
}