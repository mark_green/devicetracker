package gpx

import (
    "io/ioutil"
    "strings"
    "path/filepath"

    "bitbucket.org/mark_green/Messaging"

    "bitbucket.org/mark_green/DeviceTracker/Messages"
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type GpxDirectoryPoller struct {
    DirectoryPath string
    GetDeviceConfigs func() []model.DeviceConfig
}

func (this GpxDirectoryPoller) Poll(publisher messaging.Publisher) {
    for _, deviceConfig := range this.GetDeviceConfigs() {
        // Load up all the GPX files
        filePaths := getMatchingFilePaths(this.DirectoryPath, deviceConfig.GpxFilenamePrefix)
        gpxObjects := loadGpxObjects(filePaths)

        // Turn them into points
        points := convertGpxObjectsToPoints(deviceConfig.DeviceName, gpxObjects)

        // Publish them!
        for _, point := range points {
            publisher.Publish(messages.PointLogged {
                Point: point,
            })
        }
    }
}

func getMatchingFilePaths(directoryPath string, requiredPrefix string) []string {
    directoryFileInfos, err := ioutil.ReadDir(directoryPath)
    filePaths := make([]string, 0, len(directoryFileInfos))
    if err != nil {
        panic(err)
    }
    for _, fileInfo := range directoryFileInfos {
        filename := fileInfo.Name()
        if strings.HasSuffix(filename, ".gpx") && strings.HasPrefix(filename, requiredPrefix) {
            filePaths = append(filePaths, filepath.Join(directoryPath, filename))
        }
    }
    return filePaths
}

func loadGpxObjects(filePaths []string) []*Gpx {
    gpxObjects := make([]*Gpx, 0, len(filePaths))
    for _, filePath := range filePaths {
        gpxObject, err := Parse(filePath)
        if err != nil {
            panic(err)
        }
        gpxObjects = append(gpxObjects, gpxObject)
    }
    return gpxObjects
}
