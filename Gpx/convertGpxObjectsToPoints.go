package gpx

import (
	"time"

    "bitbucket.org/mark_green/DeviceTracker/Model"
)

func convertGpxObjectsToPoints(deviceName string, gpxObjects []*Gpx) []model.Point {
    points := make([]model.Point, 0)
    for _, gpxObject := range gpxObjects {
        morePoints := convertGpxObjectToPoints(deviceName, gpxObject)
        points = append(points, morePoints...)
    }
    return points
}

func convertGpxObjectToPoints(deviceName string, gpxObject *Gpx) []model.Point {
	points := make([]model.Point, 0)

    for _, track := range gpxObject.Tracks {
        for _, segment := range track.Segments {
            for _, point := range segment.Points {
            	timestamp, err := time.Parse("2006-01-02T15:04:05Z", point.Timestamp)
            	if err != nil {
            		panic(err)
            	}

                points = append(points, model.Point {
                    DeviceName: deviceName,
                    Timestamp: timestamp,
                    Latitude: point.Lat,
                    Longitude: point.Lon,
                    Elevation: point.Ele,
                    Speed: point.Speed,
                    Satellites: point.Sat,
                    Hdop: point.Hdop,
                    Vdop: point.Vdop,
                    Pdop: point.Pdop,
                })
            }
        }
    }

	return points
}
