// Borrows a large chunk of Peter Vasil's go-gpx
//    "github.com/ptrv/go-gpx"
// It didn't work for my purpose, so I 'fixed' it :P

package gpx

import (
	"encoding/xml"
	"os"
)

/*==========================================================*/

const TIMELAYOUT = "2006-01-02T15:04:05Z"
const DEFAULT_STOPPED_SPEED_THRESHOLD = 1.0

/*==========================================================*/

type GpxTrkseg struct {
	XMLName xml.Name `xml:"trkseg"`
	Points  []GpxWpt `xml:"trkpt"`
}

type GpxTrk struct {
	XMLName  xml.Name    `xml:"trk"`
	Name     string      `xml:"name,omitempty"`
	Cmt      string      `xml:"cmt,omitempty"`
	Desc     string      `xml:"desc,omitempty"`
	Src      string      `xml:"src,omitempty"`
	Links    []GpxLink   `xml:"link"`
	Number   int         `xml:"number,omitempty"`
	Type     string      `xml:"type,omitempty"`
	Segments []GpxTrkseg `xml:"trkseg"`
}

type GpxWpt struct {
	Lat float64 `xml:"lat,attr"`
	Lon float64 `xml:"lon,attr"`
	// Position info
	Ele         float64 `xml:"ele,omitempty"`
	Timestamp   string  `xml:"time,omitempty"`
	MagVar      string  `xml:"magvar,omitempty"`
	GeoIdHeight string  `xml:"geoidheight,omitempty"`
	Speed       float64 `xml:"speed,omitempty"`
	// Description info
	Name  string    `xml:"name,omitempty"`
	Cmt   string    `xml:"cmt,omitempty"`
	Desc  string    `xml:"desc,omitempty"`
	Src   string    `xml:"src,omitempty"`
	Links []GpxLink `xml:"link"`
	Sym   string    `xml:"sym,omitempty"`
	Type  string    `xml:"type,omitempty"`
	// Accuracy info
	Fix          string  `xml:"fix,omitempty"`
	Sat          int     `xml:"sat,omitempty"`
	Hdop         float64 `xml:"hdop,omitempty"`
	Vdop         float64 `xml:"vdop,omitempty"`
	Pdop         float64 `xml:"pdop,omitempty"`
	AgeOfGpsData float64 `xml:"ageofgpsdata,omitempty"`
	DGpsId       int     `xml:"dgpsid,omitempty"`
}

type GpxRte struct {
	XMLName     xml.Name  `xml:"rte"`
	Name        string    `xml:"name,omitempty"`
	Cmt         string    `xml:"cmt,omitempty"`
	Desc        string    `xml:"desc,omitempty"`
	Src         string    `xml:"src,omitempty"`
	Links       []GpxLink `xml:"link"`
	Number      int       `xml:"number,omitempty"`
	Type        string    `xml:"type,omitempty"`
	RoutePoints []GpxWpt  `xml:"rtept"`
}

type GpxLink struct {
	XMLName xml.Name `xml:"link"`
	Url     string   `xml:"href,attr,omitempty"`
	Text    string   `xml:"text,omitempty"`
	Type    string   `xml:"type,omitempty"`
}

type GpxCopyright struct {
	XMLName xml.Name `xml:"copyright"`
	Author  string   `xml:"author,attr"`
	Year    string   `xml:"year,omitempty"`
	License string   `xml:"license,omitempty"`
}

type GpxEmail struct {
	XMLName xml.Name `xml:"email"`
	Id      string   `xml:"id,attr,omitempty"`
	Domain  string   `xml:"domain,attr,omitempty"`
}

type GpxPerson struct {
	XMLName xml.Name  `xml:"author"`
	Name    string    `xml:"name,omitempty"`
	Email   *GpxEmail `xml:"email,omitempty"`
	Link    *GpxLink  `xml:"link,omitempty"`
}

type GpxMetadata struct {
	XMLName   xml.Name      `xml:"metadata"`
	Name      string        `xml:"name,omitempty"`
	Desc      string        `xml:"desc,omitempty"`
	Author    *GpxPerson    `xml:"author,omitempty"`
	Copyright *GpxCopyright `xml:"copyright,omitempty"`
	Links     []GpxLink     `xml:"link"`
	Timestamp string        `xml:"time,omitempty"`
	Keywords  string        `xml:"keywords,omitempty"`
	Bounds    *GpxBounds    `xml:"bounds"`
}

type Gpx struct {
	XMLName      xml.Name     `xml:"gpx"`
	XmlNsXsi     string       `xml:"xmlns:xsi,attr,omitempty"`
	XmlSchemaLoc string       `xml:"xsi:schemaLocation,attr,omitempty"`
	Version      string       `xml:"version,attr"`
	Creator      string       `xml:"creator,attr"`
	Metadata     *GpxMetadata `xml:"metadata,omitempty"`
	Waypoints    []GpxWpt     `xml:"wpt"`
	Routes       []GpxRte     `xml:"rte"`
	Tracks       []GpxTrk     `xml:"trk"`
}

type GpxBounds struct {
	XMLName xml.Name `xml:"bounds"`
	MinLat  float64  `xml:"minlat,attr"`
	MaxLat  float64  `xml:"maxlat,attr"`
	MinLon  float64  `xml:"minlon,attr"`
	MaxLon  float64  `xml:"maxlon,attr"`
}

/*==========================================================*/

// Parse parses a GPX file and return a Gpx object.
func Parse(gpxPath string) (*Gpx, error) {
	gpxFile, err := os.Open(gpxPath)
	if err != nil {
		// fmt.Println("Error opening file: ", err)
		return nil, err
	}
	defer gpxFile.Close()

	g := NewGpx()

	decoder := xml.NewDecoder(gpxFile)
	err = decoder.Decode(&g)
	if err != nil {
		// fmt.Println("Error reading file: ", err)
		return nil, err
	}

	return g, nil
}

/*==========================================================*/

// NewGpx creates and returns a new Gpx objects.
func NewGpx() *Gpx {
	gpx := new(Gpx)
	gpx.XmlNsXsi = "http://www.w3.org/2001/XMLSchema-instance"
	gpx.XmlSchemaLoc = "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"
	gpx.Version = "1.1"
	gpx.Creator = "https://github.com/ptrv/go-gpx"
	return gpx
}
