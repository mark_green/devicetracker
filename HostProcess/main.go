package main

import (
	"flag"
	"log"
    "os"
    "os/signal"
    "strings"
    "syscall"
    "bitbucket.org/mark_green/MapReduceFilter"
    "bitbucket.org/mark_green/DeviceTracker/Storage"
)

func main() {
	flagSet := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	cluster := flagSet.String("cluster", "127.0.0.1", "The addresses of the cassandra nodes, comma separated")
    configKeyspace := flagSet.String("config_keyspace", "devicetracker_config", "The name of the keyspace config is stored in")
    messagingKeyspace := flagSet.String("messaging_keyspace", "devicetracker_messaging", "The name of the keyspace messages and state are stored in")
    gpxDirectory := flagSet.String("gpx_directory", ".", "The path to the directory to poll for Gpx data")
	httpPort := flagSet.Int("httpPort", 80, "The port to serve http requests on")
	pushbulletToken := flagSet.String("pushbullet_token", "", "The token to authenticate to the pushbullet api with")
	flagSet.Parse(os.Args[1:])

    clusterIps := mapreducefilter.MapString(strings.Split(*cluster, ","), strings.TrimSpace)
    storageService := storage.NewStorageService(*configKeyspace, *messagingKeyspace, clusterIps...)

	done := make(chan struct{})
	doneReply := make(chan struct{})

	bus, runner := bootstrapBus(*pushbulletToken, storageService)
	bootstrapGpxPolling(*gpxDirectory, storageService, bus, done)
	go runner.Run(done, doneReply)
	bootstrapHttp(*httpPort, storageService)

	sigchan := make(chan os.Signal, 10)
    signal.Notify(sigchan, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTRAP, syscall.SIGQUIT)
    <-sigchan
    log.Println("Shutting down gracefully...")

    // Broadcast shutdown and wait for reply
    close(done)
    <-doneReply

    log.Println("Done!")
    os.Exit(0)
}
