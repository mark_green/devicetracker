package main

import (
	"bytes"
	"log"
	"encoding/json"
	"time"
    //"bitbucket.org/mark_green/GoPushbullet"
    "bitbucket.org/mark_green/Messaging"
    "bitbucket.org/mark_green/DeviceTracker/Core"
    "bitbucket.org/mark_green/DeviceTracker/Model"
    "bitbucket.org/mark_green/DeviceTracker/Storage"
)

func bootstrapBus(pushbulletToken string, storageService *storage.StorageService) (*messaging.Bus, *messaging.CompositeRunnable) {
	bus := messaging.NewBus() 
	//pbclient := gopushbullet.NewClient(pushbulletToken)

	transitionDetector := messaging.NewAutoProcessManagerRunner(
		core.TransitionDetector {
			GetDeviceConfig: storageService.GetDeviceConfig,
	}).WithPersister(storageService)
	transitionWatcher := messaging.NewAutoHandler(
		core.TransitionWatcher {
			GetWatches: storageService.GetWatches,
	})
	watcherNotifier := messaging.NewAutoHandler(
		core.WatcherNotifier {
			GetWatchers: storageService.GetWatchers,
			Notify: func(watcher model.Watcher, watchedTransition model.WatchedTransition) {
				var b1, b2 bytes.Buffer
				encoder := json.NewEncoder(&b1)
				encoder.Encode(watcher)
				encoder = json.NewEncoder(&b2)
				encoder.Encode(watchedTransition)
				log.Println("Notifying", b1.String(), b2.String())
				//pbclient.PushTo(gopushbullet.Email{watcher.PersonEmail}, gopushbullet.Note{"GoClientTest", "This should be the body"})
			},
	})

	scheduledMessageHandler := messaging.NewScheduledMessageHandler(time.Duration(50)*time.Millisecond)

	bus.AutoSubscribe(transitionDetector, transitionWatcher, watcherNotifier, scheduledMessageHandler)
	//bus.SubscribeTo([]string{"TransitionDetected"}, messaging.LoggingHandler{})

	runner := messaging.NewCompositeRunnable(scheduledMessageHandler)

	return bus, runner
}