package main

import (
	"fmt"
	"log"
	"net/http"
	"bitbucket.org/mark_green/DeviceTracker/HttpApi"
	"bitbucket.org/mark_green/DeviceTracker/Storage"
)

func bootstrapHttp(httpPort int, storageService *storage.StorageService) {
	httpServeMux := http.NewServeMux()
    httpServeMux.Handle("/", http.FileServer(http.Dir("./HttpStatic")))
	httpApi.RegisterRoutes(httpServeMux, storageService)
	go func() {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", httpPort), httpServeMux))
	}()
}