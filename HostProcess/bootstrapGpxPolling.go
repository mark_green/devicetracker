package main

import (
	"time"
	"bitbucket.org/mark_green/Messaging"
	"bitbucket.org/mark_green/DeviceTracker/Gpx"
	"bitbucket.org/mark_green/DeviceTracker/Storage"
)

func bootstrapGpxPolling(gpxDirectory string, storageService *storage.StorageService, bus *messaging.Bus, done <-chan struct{}) {
	go func() {
		ticker := time.NewTicker(time.Duration(5)*time.Second)
		poller := gpx.GpxDirectoryPoller {
			GetDeviceConfigs: storageService.GetDeviceConfigs,
			DirectoryPath: gpxDirectory,
		}
		for {
			select {
			case <- ticker.C:
				poller.Poll(bus)
			case <- done:
				ticker.Stop()
				return
			}
		}
	}()

}