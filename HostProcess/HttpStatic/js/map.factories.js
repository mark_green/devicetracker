'use strict';

angular.module('map.factories', [])
	.factory('apiService', ['$http', function($http) {
		return {
			GetDevices: function (continuation) {
				$http({method: 'GET', url: '/api/devices'}).
				    success(function(data, status, headers, config) {
				      continuation(data);
				    });
			},

			GetDevicePoints: function (device, from, to, continuation) {
				$http({method: 'POST', url: '/api/points', data: {
				        "Device": device,
				        "From": from,
				        "To": to
				      }}).
				    success(function(data, status, headers, config) {
				      continuation(data);
				    });
			},

			GetDeviceWatches: function (device, continuation) {
				$http({method: 'POST', url: '/api/watches', data: {
				        "Device": device,
				      }}).
				    success(function(data, status, headers, config) {
				      continuation(data);
				    });
			},

			GetDeviceTransitions: function (device, from, to, continuation) {
				$http({method: 'POST', url: '/api/transitions', data: {
				        "Device": device,
				        "From": from,
				        "To": to
				      }}).
				    success(function(data, status, headers, config) {
				      continuation(data);
				    });
			},

			GetDeviceWatchedTransitions: function (device, from, to, continuation) {
				$http({method: 'POST', url: '/api/watchedtransitions', data: {
				        "Device": device,
				        "From": from,
				        "To": to
				      }}).
				    success(function(data, status, headers, config) {
				      continuation(data);
				    });
			}
		};
	}])
	.factory('queryStringService', [function() {
		function get(name, defaultValue) {
		        return getList(name, [defaultValue])[0];
		};

		function getList (name, defaultValue) {
	        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	        var regex = new RegExp("[&;]?" + name + "=([^&;]+)", "g");
	        var results = [];
	        var matches;
	        while(matches = regex.exec(location.search)) {
	          results.push(decodeURIComponent(matches[1]));
	        }
	        if(results.length == 0) {
	          return defaultValue;
	        }
	        return results;
	    };

		return {
			Get: get,
		    GetList: getList 
	    };
	}]);