'use strict';

angular.module('map.controllers', ['map.factories'])
    .controller('viewCtrl', ['$scope', 'apiService', 'queryStringService', 'mapElement', function($scope, apiService, queryStringService, mapElement) {
	  	
	    $scope.map = new google.maps.Map(mapElement, {
			zoom: 11,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	    });

	    $scope.infoWindow = new google.maps.InfoWindow({
			content: ''
		});

	    $scope.showMarkerInfo = function(objectWithMarkerProperty) {
	    	var forSerialising = {};
	    	for(var prop in objectWithMarkerProperty) {
	    		if(prop != 'Marker') {
	    			forSerialising[prop] = objectWithMarkerProperty[prop];
	    		}
	    	}
	    	$scope.infoWindow.setContent(JSON.stringify(forSerialising, undefined, 2).replace(/\n/g,'<br/>\n').replace(/\s/g,'&nbsp;'));
	    	$scope.infoWindow.open($scope.map, objectWithMarkerProperty.Marker);
	    }

    	$scope.focusOn = function (point) {
			$scope.map.panTo(new google.maps.LatLng(point.Latitude, point.Longitude));
    	};

    	$scope.formatTimestamp = function(timestamp) {
    		return new Date(timestamp).toLocaleString();
    	}

		apiService.GetDevices(function(response) {
			$scope.devices = response.Devices;
			var device = $scope.devices[queryStringService.Get('device', 0)];
			$scope.map.setCenter(new google.maps.LatLng(device.Latitude, device.Longitude));
			device.Marker = new google.maps.Marker({
		        position: new google.maps.LatLng(device.Latitude, device.Longitude),
		        map: $scope.map,
		        title: device.DeviceName,
		        icon: {
				    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
				    scale: 8
			    }
		    });
		
		    var from = new Date();
		    var to = new Date();
		    from.setDate(from.getDate()-Number(queryStringService.Get('days', 1)));
		    apiService.GetDevicePoints(device.DeviceName, from, to, function(response) {
				$scope.points = response.Points;
				var polyline = new google.maps.Polyline({
					path: response.Points.map(function(point) {
						return new google.maps.LatLng(point.Latitude, point.Longitude);
					}),
					strokeColor: '#FF0000',
					strokeOpacity: 1.0,
					strokeWeight: 2
				});
				$scope.polyline = polyline;
				polyline.setMap($scope.map);

				$scope.path = polyline.getPath();
	    	});

	    	apiService.GetDeviceWatches(device.DeviceName, function(response) {
	    		$scope.watches = response.Watches;
				for (var i = $scope.watches.length - 1; i >= 0; i--) {
					var watch = $scope.watches[i];
					watch.Circle = new google.maps.Circle({
						strokeColor: '#FF0000',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#FF0000',
						fillOpacity: 0.35,
						map: $scope.map,
						center: new google.maps.LatLng(watch.Latitude, watch.Longitude),
						radius: watch.RangeInDegrees * 110922
				    });
				}
	    	});

	   //  	apiService.GetDeviceTransitions(device.DeviceName, from, to, function(response) {
				// $scope.transitions = response.Transitions;
				// for (var i = $scope.transitions.length - 1; i >= 0; i--) {
				// 	var transition = $scope.transitions[i];
				// 	transition.Marker = new google.maps.Marker({
				//         position: new google.maps.LatLng(transition.Point.Latitude, transition.Point.Longitude),
				//         map: $scope.map,
				//         title: device.DeviceName + ' ' + transition.Transition + ' ' + $scope.formatTimestamp(transition.Point.Timestamp)
				//     });
				// }
	   //  	});

	    	apiService.GetDeviceWatchedTransitions(device.DeviceName, from, to, function(response) {
	    		$scope.watchedTransitions = response.WatchedTransitions;
	    		for (var i = $scope.watchedTransitions.length - 1; i >= 0; i--) {
					var watchedTransition = $scope.watchedTransitions[i];
					watchedTransition.Marker = new google.maps.Marker({
				        position: new google.maps.LatLng(watchedTransition.Point.Latitude, watchedTransition.Point.Longitude),
				        map: $scope.map,
				        title: device.DeviceName + ' ' + watchedTransition.Watch.WatchType + ' ' + $scope.formatTimestamp(watchedTransition.Point.Timestamp)
				    });
				}
	    	});
		});
	}])
	.controller('replayCtrl', ['$scope', 'apiService', 'queryStringService', 'mapElement', function($scope, apiService, queryStringService, mapElement) {
	  	
	    $scope.map = new google.maps.Map(mapElement, {
			zoom: 11,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	    });
	  
		apiService.GetDevices(function(response) {
			$scope.devices = response.Devices;
			var device = $scope.devices[queryStringService.Get('device', 0)];
			$scope.map.setCenter(new google.maps.LatLng(device.Latitude, device.Longitude));
			new google.maps.Marker({
		        position: new google.maps.LatLng(device.Latitude, device.Longitude),
		        map: $scope.map,
		        title: device.DeviceName,
		        icon: {
				    path: google.maps.SymbolPath.CIRCLE,
				    scale: 10
			    }
		    });

		    var from = new Date();
		    var to = new Date();
		    from.setDate(from.getDate()-Number(queryStringService.Get('days', 1)));
		    apiService.GetDevicePoints(device.DeviceName, from, to, function(response) {
				$scope.points = response.Points;
				var polyline = new google.maps.Polyline({
					path: [],
					strokeColor: '#FF0000',
					strokeOpacity: 1.0,
					strokeWeight: 2
				});
				$scope.polyline = polyline;
				polyline.setMap($scope.map);

				$scope.path = polyline.getPath();
				$scope.pathRemaining = response.Points;
				$scope.time = new Date($scope.pathRemaining[$scope.pathRemaining.length-1].Timestamp);
				$scope.consecutiveMisses = 0;
				var interval = setInterval(function() {
					$scope.$apply(function () {
						if($scope.pathRemaining.length == 0) {
							clearInterval(interval);
							return;
						}

				        var point = $scope.pathRemaining.pop();
				        if($scope.time > new Date(point.Timestamp)) {
			        		$scope.path.push(new google.maps.LatLng(point.Latitude, point.Longitude));
			        		$scope.consecutiveMisses = 0;
			        	} else {
			        		$scope.pathRemaining.push(point);
			        		$scope.consecutiveMisses++;
			        	}
			        	$scope.time = new Date($scope.time.getTime()+60*1000);
					});
	    		}, 50);
	    	});
		});
	}]);