package messages

import (
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type NotifyWatcher struct {
	Watcher model.Watcher
	WatchedTransition model.WatchedTransition
}

func (this NotifyWatcher) GetTopics() []string {
	return []string{}
}