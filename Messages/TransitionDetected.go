package messages

import (
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type TransitionDetected struct {
	Transition model.Transition
}

func (this TransitionDetected) GetTopics() []string {
	return []string{}
}