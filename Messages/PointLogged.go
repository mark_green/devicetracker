package messages

import (
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type PointLogged struct {
	Point model.Point
}

func (this PointLogged) GetTopics() []string {
	return []string{}
}