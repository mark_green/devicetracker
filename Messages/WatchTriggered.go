package messages

import (
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type WatchTriggered struct {
	WatchedTransition model.WatchedTransition
}

func (this WatchTriggered) GetTopics() []string {
	return []string{}
}