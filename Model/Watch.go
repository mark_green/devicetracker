package model

type WatchType string

const (
	WatchType_Arrives WatchType = "arrives"
	WatchType_Leaves  WatchType = "leaves"
)

type Watch struct {
	DeviceName string
	WatchName string
	WatchType WatchType
	Latitude float64
	Longitude float64
	RangeInDegrees float64
}
