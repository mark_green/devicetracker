package model

type Watcher struct {
	DeviceName string
	WatchName string
	PersonName string
	PersonEmail string
}
