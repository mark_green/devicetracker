package model

type WatchedTransition struct {
	Watch Watch
	Transition Transition
}
