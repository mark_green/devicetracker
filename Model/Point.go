package model

import (
	"time"
)

type Point struct {
	DeviceName string
	Timestamp time.Time
	Latitude float64
	Longitude float64
	Elevation float64
	Speed float64
	Satellites int
	Hdop float64
	Vdop float64
	Pdop float64
}
