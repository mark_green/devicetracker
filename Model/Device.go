package model

import (
	"time"
)

type Device struct {
	DeviceName string
	Timestamp time.Time
	Latitude float64
	Longitude float64
}
