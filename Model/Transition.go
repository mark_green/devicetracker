package model

import (
	"time"
)

type TransitionType string
const (
	TransitionType_Arrive TransitionType = "arrive"
	TransitionType_Leave  TransitionType = "leave"
)

type Transition struct {
	TransitionType TransitionType
	DetectedAt time.Time
	Point Point
	WatchPoint Point
}