package model

type DeviceConfig struct {
	DeviceName string
	GpxFilenamePrefix string
	MaxRangeInDegreesForStop float64
	RequiredPointsToTransition int
}

func DefaultDeviceConfig(deviceName string) DeviceConfig {
	return DeviceConfig {
		DeviceName: deviceName,
		GpxFilenamePrefix: "",
		MaxRangeInDegreesForStop: 0.002,
		RequiredPointsToTransition: 4,
	}
}
