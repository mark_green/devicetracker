package core

import (
	"time"
    
    "bitbucket.org/mark_green/Messaging"

    "bitbucket.org/mark_green/DeviceTracker/Messages"
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type TransitionDetector struct {
	GetDeviceConfig func(deviceName string) model.DeviceConfig
}

type TransitionDetectorState struct {
	DeviceConfig model.DeviceConfig
	CheckpointTimestamp time.Time
	Stopped bool
	Trail *Trail
}

func (this TransitionDetector) GetProcessName() string {
	return "TransitionDetector"
}

func (this TransitionDetector) NewState(correlation string) messaging.ProcessManagerState {
	deviceConfig := this.GetDeviceConfig(correlation)

	return TransitionDetectorState {
		DeviceConfig: deviceConfig,
		Trail: NewTrail(deviceConfig.RequiredPointsToTransition),
		Stopped: false,
	}
}

func (this TransitionDetector) GetCorrelationFor(message messaging.Message) string {
	switch message.(type) { 
    case messages.PointLogged:
        return message.(messages.PointLogged).Point.DeviceName
    }
    return ""
}

func (this TransitionDetector) HandlePointLogged(state TransitionDetectorState, message messages.PointLogged, publisher messaging.Publisher) TransitionDetectorState {
	if !message.Point.Timestamp.After(state.CheckpointTimestamp) {
		return state
	}
	state.CheckpointTimestamp = message.Point.Timestamp
	state.Trail.Push(message.Point)

	if state.Trail.IsFull() {
		if !state.Stopped && state.Trail.All(areNearerThan(state.DeviceConfig.MaxRangeInDegreesForStop)) {
			publisher.Publish(
				messages.TransitionDetected {
					Transition: model.Transition {
						TransitionType: model.TransitionType_Arrive,
						DetectedAt: message.Point.Timestamp,
						Point: state.Trail.Points[0],
						WatchPoint: state.Trail.Points[1],
					},
				})
			state.Stopped = true
		}

		if state.Stopped && state.Trail.All(areFurtherThan(state.DeviceConfig.MaxRangeInDegreesForStop)) {
			publisher.Publish(
				messages.TransitionDetected {
					Transition: model.Transition {
						TransitionType: model.TransitionType_Leave,
						DetectedAt: message.Point.Timestamp,
						Point: state.Trail.Points[1],
						WatchPoint: state.Trail.Points[0],
					},
				})
			state.Stopped = false
		}
	}

	return state
}

func areNearerThan(maxRangeInDegrees float64) func(point model.Point) bool {
	var firstPoint *model.Point
	return func(point model.Point) bool {
		if firstPoint == nil {
			firstPoint = &point
			return true
		} else {
			return rangeInDegrees(*firstPoint, point) < maxRangeInDegrees
		}
	}
}

func areFurtherThan(maxRangeInDegrees float64) func(point model.Point) bool {
	var firstPoint *model.Point
	return func(point model.Point) bool {
		if firstPoint == nil {
			firstPoint = &point
			return true
		} else {
			return rangeInDegrees(*firstPoint, point) >= maxRangeInDegrees
		}
	}
}
