package core


import (
    "bitbucket.org/mark_green/Messaging"

    "bitbucket.org/mark_green/DeviceTracker/Messages"
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type WatcherNotifier struct {
	GetWatchers func(deviceName string, watchName string) []model.Watcher
	Notify func(watcher model.Watcher, watchedTransition model.WatchedTransition) 
}

func (this WatcherNotifier) HandleWatchTriggered(message messages.WatchTriggered, publisher messaging.Publisher) {
	watchers := this.GetWatchers(message.WatchedTransition.Watch.DeviceName, message.WatchedTransition.Watch.WatchName)
	for _, watcher := range watchers {
		publisher.Publish(messages.NotifyWatcher {
			Watcher: watcher,
			WatchedTransition: message.WatchedTransition,
		})
	}
}

func (this WatcherNotifier) HandleNotifyWatcher(message messages.NotifyWatcher, publisher messaging.Publisher) {
	this.Notify(message.Watcher, message.WatchedTransition)
}
