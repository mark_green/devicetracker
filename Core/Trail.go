package core

import (
	"log"
    
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type Trail struct {
	Points []model.Point
	MaxLength int
}

func NewTrail(length int) *Trail {
	return &Trail {
		Points: make([]model.Point, length),
		MaxLength: length,
	}
}

func (trail *Trail) IsFull() bool {
	return len(trail.Points) == trail.MaxLength
}

func (trail *Trail) Push(point model.Point) {
	if !trail.IsFull() {
		trail.Points = append(trail.Points, point)
	} else {
		for i:= 0; i<trail.MaxLength-1; i++ {
			trail.Points[i] = trail.Points[i+1]
		}
		trail.Points[trail.MaxLength-1] = point
	}
}

func (trail *Trail) All(test func(model.Point) bool) bool {
	if !trail.IsFull() {
		log.Println("Attempt to call .All on a non-full trail")
		return false
	}
	for _, point := range trail.Points {
		if !test(point) {
			return false
		}
	}
	return true
}

func (trail *Trail) Any(test func(model.Point) bool) bool {
	if !trail.IsFull() {
		log.Println("Attempt to call .Any on a non-full trail")
		return false
	}
	for _, point := range trail.Points {
		if test(point) {
			return true
		}
	}
	return false
}
