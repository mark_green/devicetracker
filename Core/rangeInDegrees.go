package core

import (
	"math"
	
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

func rangeInDegrees(point1 model.Point, point2 model.Point) float64 {
	dlon := point2.Longitude - point1.Longitude 
	dlat := point2.Latitude - point1.Latitude
	return math.Sqrt(dlon*dlon + dlat*dlat)
}
