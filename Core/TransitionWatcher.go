package core

import (
    "bitbucket.org/mark_green/Messaging"
    "bitbucket.org/mark_green/DeviceTracker/Messages"
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type TransitionWatcher struct {
	GetWatches func(deviceName string) []model.Watch
}

func (this TransitionWatcher) HandleTransitionDetected(message messages.TransitionDetected, publisher messaging.Publisher) {
	watches := this.GetWatches(message.Transition.Point.DeviceName)
	
	for _, watch := range watches {
		if watchMatchesTransition(watch, message.Transition) {
			publisher.Publish(messages.WatchTriggered {
					WatchedTransition: model.WatchedTransition {
						Watch: watch,
						Transition: message.Transition,
					},
				})
		}
	}
}

func watchMatchesTransition(watch model.Watch, transition model.Transition) bool {
	switch(watch.WatchType) {
	case model.WatchType_Leaves:
		if transition.TransitionType == model.TransitionType_Leave {
			return rangeInDegrees(transition.WatchPoint, model.Point{
				Latitude: watch.Latitude,
				Longitude: watch.Longitude,
			}) <= watch.RangeInDegrees
		}

	case model.WatchType_Arrives:
		if transition.TransitionType == model.TransitionType_Arrive {
			return rangeInDegrees(transition.WatchPoint, model.Point{
				Latitude: watch.Latitude,
				Longitude: watch.Longitude,
			}) <= watch.RangeInDegrees
		}
	}

	return false
}