package httpApi

import (
    "net/http"
    "bitbucket.org/mark_green/HttpRpc"
    "bitbucket.org/mark_green/DeviceTracker/Storage"
)

func RegisterRoutes(mux *http.ServeMux, storageService *storage.StorageService) {
	apiService := NewApiService(storageService)

    mux.HandleFunc("/api/devices", httpRpc.HandleErrorsAnd(httpRpc.HandleHttpGet(apiService.GetDevices)))
    mux.HandleFunc("/api/points", httpRpc.HandleErrorsAnd(httpRpc.HandleHttpPost(apiService.GetPoints)))
    mux.HandleFunc("/api/watches", httpRpc.HandleErrorsAnd(httpRpc.HandleHttpPost(apiService.GetWatches)))
    mux.HandleFunc("/api/transitions", httpRpc.HandleErrorsAnd(httpRpc.HandleHttpPost(apiService.GetTransitions)))
    mux.HandleFunc("/api/watchedtransitions", httpRpc.HandleErrorsAnd(httpRpc.HandleHttpPost(apiService.GetWatchedTransitions)))
}
