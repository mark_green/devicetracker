package httpApi

import (
	"time"
    
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type GetWatchedTransitionsRequest struct {
	Device string
	From time.Time
	To time.Time
}
type GetWatchedTransitionsResponse struct {
	WatchedTransitions []model.WatchedTransition
}
func (this *apiService) GetWatchedTransitions(req *GetWatchedTransitionsRequest) GetWatchedTransitionsResponse {
	return GetWatchedTransitionsResponse {
		//WatchedTransitions: watchedTransitions,
	}
}
