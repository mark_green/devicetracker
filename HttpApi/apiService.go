package httpApi

import (
    "bitbucket.org/mark_green/DeviceTracker/Storage"
)

type apiService struct {
    storageService *storage.StorageService
}

func NewApiService(storageService *storage.StorageService) (instance *apiService) {
    instance = new (apiService)

    instance.storageService = storageService

    return
}