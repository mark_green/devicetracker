package httpApi

import (
    
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type GetWatchesRequest struct {
	Device string
}
type GetWatchesResponse struct {
	Watches []model.Watch
}
func (this *apiService) GetWatches(req *GetWatchesRequest) GetWatchesResponse {
	return GetWatchesResponse {
        Watches: this.storageService.GetWatches(req.Device),
    }
}
