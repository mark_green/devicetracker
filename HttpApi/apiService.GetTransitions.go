package httpApi

import (
	"time"
    
    "bitbucket.org/mark_green/DeviceTracker/Model"
)

type GetTransitionsRequest struct {
	Device string
	From time.Time
	To time.Time
}
type GetTransitionsResponse struct {
	Transitions []model.Transition
}
func (this *apiService) GetTransitions(req *GetTransitionsRequest) GetTransitionsResponse {
	return GetTransitionsResponse {
		//Transitions: transitions,
	}
}