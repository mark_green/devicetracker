# DeviceTracker #

A framework for notifying devices about the location of other devices

### What's this for? ###

The goal here is to provide a framework for notifying people when other people move around.

I've tried to get this working the way I want it with various other services, and never got exactly what I wanted.

### How does it work? ###

The basic architecture:

* Cassandra for storing device location history, and config like which places you care about
* PushBullet is used for notifications

### Go dependencies ###

Run these and you should be able to build this repo

	go get github.com/gocql/gocql
	go get bitbucket.org/mark_green/HttpRpc
	go get bitbucket.org/mark_green/MapReduceFilter
	go get bitbucket.org/mark_green/Messaging